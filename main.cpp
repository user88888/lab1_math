#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <cmath>
#include <limits>

typedef struct range {
	double L;
	double H;
};

typedef struct rangeClass {
	std::vector<range> _range;
};

typedef struct n_c_range_class_count {
	int count;
	double f_b_probability;
	double f_b_possibility;
	double probability;
	std::vector<double> matrix_row;
};

typedef struct possibility_data {
	ptrdiff_t C_L;	// level function result
	double L_f;		// level set
	double delta_l; // l_{k+1} - l_{k}
	double fuzz;	// fuzzy function result
};

#define matrix_t std::vector<std::vector<double>>

matrix_t matrix = {
	{ 2.72,2.41,19.97 },
	{ 1.55,3.34,27.34 },
	{ 1.64,2.32,16.68 },
	{ 1.74,2.85,22.25 },
	{ 1.16,2.95,22.22 },
	{ 1.14,3.68,30.66 },
	{ 1.12,2.59,18.33 },
	{ 2.83,2.74,23.61 },
	{ 2.55,2.66,22.04 },
	{ 2.4,3.69,33.33 },
	{ 2.44,2.61,21.32 },
	{ 1.33,2.66,19.42 },
	{ 1.93,2.49,18.95 },
	{ 2.65,2.56,21.31 },
	{ 1.96,2.3,17.12 },
	{ 2.75,2.57,21.73 },
	{ 2.55,3.95,37 },
	{ 1.99,3.78,33.46 },
	{ 2.65,2.04,16.38 },
	{ 1.28,2.29,15.66 },
	{ 2,2.04,14.84 },
	{ 2.19,2.02,15.05 },
	{ 2.55,3.3,29.1 },
	{ 2.54,3.42,30.42 },
	{ 2.12,2.41,18.55 },
	{ 2.36,3.19,27.32 },
	{ 2.91,3.29,29.89 },
	{ 3,2.49,21.52 },
	{ 2.35,2.59,20.9 },
	{ 1.17,3.54,29.05 },
};

matrix_t matrixOut;

std::vector<double> mins, maxs;

std::vector<rangeClass> ranges;

std::vector<n_c_range_class_count> p_sequence;

std::vector<possibility_data> p_data_sequence;

void find_min_max
(
	const std::vector<std::vector<double>>& matrix,
	std::vector<double>& mins,
	std::vector<double>& maxs
)
{

	double cmp_max, cmp_min;
	int collSize = matrix[0].size();

	cmp_max = -1.0 * 0xFFFFFFFF;
	cmp_min = 1.0 * 0xFFFFFFFF;

	maxs.resize(collSize);
	mins.resize(collSize);

	// FIND MAX
	for (ptrdiff_t j = 0; j < collSize; ++j)
	{
		double _max = cmp_max;
		for (ptrdiff_t i = 0; i < matrix.size(); ++i)
		{
			if (matrix[i][j] > _max) _max = matrix[i][j];
		}
		maxs[j] = _max;
	}

	// FIND MIN
	for (ptrdiff_t j = 0; j < collSize; ++j)
	{
		double _min = cmp_min;
		for (ptrdiff_t i = 0; i < matrix.size(); ++i)
		{
			if (matrix[i][j] < _min) _min = matrix[i][j];
		}
		mins[j] = _min;
	}
}

void calc_ranges(
	std::vector<rangeClass>& ranges,
	const std::vector<double>& mins,
	const std::vector<double>& maxs)
{
	ptrdiff_t classes_count = mins.size();
	ptrdiff_t beg, end;
	beg = 0;
	end = classes_count - 1;

	ranges.resize(classes_count);
	for (int i = 0; i < ranges.size(); ++i)
		ranges[i]._range.resize(classes_count);

	for (ptrdiff_t j = 0; j < mins.size(); ++j)
	{
		double rate = (maxs[j] - mins[j]) / classes_count;

		ranges[j]._range[beg].L = mins[j]; // define min
		ranges[j]._range[end].H = maxs[j]; // define max

		for (ptrdiff_t i = 0; i < ranges[j]._range.size(); ++i)
		{
			if (i == beg)
			{
				ranges[j]._range[i].H = ranges[j]._range[i].L + rate;
				continue;
			}
			else if (i < end)
			{
				ranges[j]._range[i].L = ranges[j]._range[i - 1].H;
				ranges[j]._range[i].H = ranges[j]._range[i].L + rate;
				continue;
			}
			else
			{
				ranges[j]._range[i].L = ranges[j]._range[i - 1].H;
			}
		}

	}
}

ptrdiff_t getRangeClass(
	const double value,
	const ptrdiff_t j,
	const std::vector<rangeClass>& ranges)
{
	ptrdiff_t range_class = 0;

	for (ptrdiff_t n = 0; n < ranges[j]._range.size(); ++n) {
		if (ranges[j]._range[n].L <= value && ranges[j]._range[n].H >= value)
		{
			range_class = n;
			break;
		}
	}

	return range_class;
}

void calc_class(
	const matrix_t& matrix,
	const std::vector<rangeClass>& ranges,
	matrix_t& matrixOut,
	const std::vector<double>& mins,
	const std::vector<double>& maxs)
{
	for (ptrdiff_t i = 0; i < matrix.size(); ++i)
	{
		for (ptrdiff_t j = 0; j < matrix[i].size(); ++j)
		{
			// check range
			matrixOut[i][j] = getRangeClass(matrix[i][j], j, ranges);
		}
	}
}

bool cmp_row(matrix_t& r, ptrdiff_t row_a, ptrdiff_t row_b) {

	ptrdiff_t row_size = r[0].size();

	for (ptrdiff_t i = 0; i < row_size; ++i) {
		if (r[row_a][i] != r[row_b][i]) {
			return false;
		}
	}

	return true;
}

double calc_probability(std::vector<n_c_range_class_count>& p, const matrix_t& r) {

	matrix_t r_copy = r;
	double result = 0.0;

	for (ptrdiff_t j = 0; j < r_copy.size(); ++j)
	{
		ptrdiff_t cmp_result = 1;
		for (ptrdiff_t i = 0; i < r_copy.size(); ++i) {
			if (j == i) {
				continue;
			}
			if (cmp_row(r_copy, j, i)) {
				r_copy.erase(r_copy.begin() + i);
				cmp_result++;
				if (i > 0) --i;
			}
		}

		n_c_range_class_count n_c;
		n_c.count = cmp_result;
		n_c.matrix_row = r_copy[j];
		p.push_back(n_c);
	}

	ptrdiff_t _max = 0;

	for (ptrdiff_t i = 0; i < p.size(); ++i) {
		if (p[i].count > _max) _max = p[i].count;
	}

	for (ptrdiff_t i = 0; i < p.size(); ++i) {
		p[i].f_b_probability = 1.0 * p[i].count / r.size();
		p[i].f_b_possibility = 1.0 * p[i].count / _max;
		p[i].probability = -1.0 * p[i].f_b_probability * std::log(p[i].f_b_probability);
		result += p[i].probability;
	}

	return result * 1.0 / std::log(2);
}

double calc_possibility(std::vector<n_c_range_class_count> &_prob, std::vector<possibility_data> &_poss) {

	// sort _prob data

	for (ptrdiff_t i = 0; i < _prob.size(); ++i) 
	{
		for (ptrdiff_t j = 0; j < _prob.size(); ++j) {
			if (i == j) continue;

			if (_prob[i].f_b_possibility > _prob[j].f_b_possibility) 
			{
				double fb_possibility_temp			= _prob[j].f_b_possibility;
				double fb_probability_temp			= _prob[j].f_b_probability;
				double probability_temp				= _prob[j].probability;
				int count_temp						= _prob[j].count;
				std::vector<double> matrix_row_temp = _prob[j].matrix_row;
				
				_prob[j].f_b_possibility	= _prob[i].f_b_possibility;
				_prob[j].f_b_probability	= _prob[i].f_b_probability;
				_prob[j].probability		= _prob[i].probability;
				_prob[j].count				= _prob[i].count;
				_prob[j].matrix_row			= _prob[i].matrix_row;

				_prob[i].f_b_possibility	= fb_possibility_temp;
				_prob[i].f_b_probability	= fb_probability_temp;
				_prob[i].probability		= probability_temp;
				_prob[i].count				= count_temp;
				_prob[i].matrix_row			= matrix_row_temp;

			}
		}
	}

	for (ptrdiff_t 
		i = _prob.size() - 1, 
		j = _prob.size(); i >= 0; --i, --j) 
	{
		possibility_data p_data;

		if (_poss.size() != 0)
		{
			if (_prob[i].f_b_possibility == _prob[i + 1].f_b_possibility) {
				continue;
			}

			p_data.C_L = j;
			p_data.L_f = _prob[i].f_b_possibility;
			p_data.delta_l = std::abs(_prob[i + 1].f_b_possibility - _prob[i].f_b_possibility);
			p_data.fuzz = p_data.delta_l * std::log(std::abs(p_data.C_L));
			_poss.push_back(p_data);
			continue;
		}

		p_data.C_L = j;
		p_data.L_f = _prob[i].f_b_possibility;
		p_data.delta_l = p_data.L_f;
		p_data.fuzz = p_data.delta_l * std::log(std::abs(p_data.C_L));
		_poss.push_back(p_data);
	}

	double result = 0.0;

	for (ptrdiff_t i = 0; i < _poss.size(); ++i)
	{
		result += _poss[i].fuzz;
	}

	return result / std::log(2);
}

int main() {
	std::cout << "DATA" << std::endl;
	for (ptrdiff_t i = 0; i < matrix.size(); ++i) {
		for (ptrdiff_t j = 0; j < matrix[i].size(); ++j) {
			std::cout << matrix[i][j] << "\t";
		}
		std::cout << std::endl;
	}

	std::vector<double> mins, maxs;

	find_min_max(matrix, mins, maxs);

	// SHOW RESULTS
	std::cout << "\nMins:" << std::endl;
	for (ptrdiff_t i = 0; i < mins.size(); ++i) {
		std::cout << mins[i] << "\t";
	}
	std::cout << "\n" << std::endl;

	std::cout << "Maxs:" << std::endl;
	for (ptrdiff_t i = 0; i < maxs.size(); ++i) {
		std::cout << maxs[i] << "\t";
	}
	std::cout << "\n" << std::endl;

	// FIND RANGE
	matrixOut.resize(matrix.size());
	for (ptrdiff_t i = 0; i < matrixOut.size(); ++i)
		matrixOut[i].resize(matrix[0].size());

	// CALC RANGES

	calc_ranges(ranges, mins, maxs);

	for (ptrdiff_t i = 0; i < ranges.size(); ++i) {
		std::cout << "range class: " << i << std::endl;
		for (ptrdiff_t j = 0; j < ranges[i]._range.size(); ++j) {
			std::cout << ranges[i]._range[j].L << "\t" << ranges[i]._range[j].H << std::endl;
		}
		std::cout << std::endl;
	}

	calc_class(matrix, ranges, matrixOut, mins, maxs);

	// SHOW MATRIX OUT

	std::cout << "MATRIX OUT" << std::endl;
	for (ptrdiff_t i = 0; i < matrixOut.size(); ++i) {
		for (ptrdiff_t j = 0; j < matrixOut[i].size(); ++j) {
			std::cout << matrixOut[i][j] << "\t";
		}
		std::cout << std::endl;
	}

	// CALC PROBABILITY
	double probability = calc_probability(p_sequence, matrixOut);

	// SHOW PROBABILITY
	std::cout << "PROBABILITY" << std::endl;
	for (ptrdiff_t i = 0; i < p_sequence.size(); ++i) {
		std::cout << p_sequence[i].count << "\t"
			<< p_sequence[i].f_b_probability << "\t"
			<< p_sequence[i].f_b_possibility << "\t"
			<< p_sequence[i].probability << "\t";
		for (ptrdiff_t j = 0; j < p_sequence[i].matrix_row.size(); ++j) {
			std::cout << p_sequence[i].matrix_row[j] << "\t";
		}
		std::cout << std::endl;
	}

	std::cout << "prob result " << probability << std::endl;


	// CALC POSSIBILITY
	double possiblity = calc_possibility(p_sequence, p_data_sequence);

	// SHOW POSSIBILITY
	std::cout << "SHOW POSSIBILITY" << std::endl;
	for (ptrdiff_t i = 0; i < p_data_sequence.size(); ++i) {
		std::cout << p_data_sequence[i].C_L << "\t"
			<< p_data_sequence[i].L_f << "\t"
			<< p_data_sequence[i].delta_l << "\t"
			<< p_data_sequence[i].fuzz << std::endl;
	}

	std::cout << "poss result " << possiblity << std::endl;

#ifdef _WIN32
	system("pause");
#endif

	return(0);
}
